#!/usr/bin/python

import datetime
import subprocess
import sys
import uuid
import os
import base64

def get_arg(name, default):
    for arg in sys.argv:
        if arg.startswith(name + '='):
            return arg.split('=')[1]
    return default

def call(cmd, shell=True):
    print('Calling ' + cmd)
    rc = subprocess.call(cmd, shell=True)
    #rc = p.wait()
    if rc != 0:
        print('\tFAILED!')
    return rc

workdir = get_arg('workdir', None)
if workdir is None:
    print('No workdir!')
    sys.exit(1)

os.chdir(workdir)

packages_dir = '/tmp/starsheep-build'

build_version = get_arg('build', '1')
package_version = get_arg('version', '0.1') + '.' + build_version
build_date = datetime.datetime.today().strftime('%Y%m%d%H%M')

builddir = '/tmp/starsheep'
bindir = builddir + '/usr/bin'
srcdir = builddir + '/usr/src/starsheep'
etcdir = builddir + '/etc/starsheep'
systemddir = builddir + '/etc/systemd/system'

flavors = get_arg('flavors', 'deb,rpm,tar').split(',')
print('Flavors are ' + ' '.join(flavors))

arch = get_arg('arch', 'x86_64-linux-gnu')
arch_name = get_arg('arch_name', 'x86_64')
print('Arch is ' + arch + '(' + arch_name + ')')

cores = get_arg('cores', '4')
print('Compiling on ' + str(cores) + ' cores')

sync = get_arg('sync', 'False')

print('Sync with repository: ' + sync)

call('mkdir -p ' + builddir, shell=True)
call('mkdir -p ' + bindir, shell=True)
call('mkdir -p ' + etcdir + '/keys', shell=True)
call('mkdir -p ' + systemddir, shell=True)

call('make clean', shell=True)
call('rm -rf Makefile CMakeFiles CMakeCache.txt cmake_install.cmake', shell=True)

version = open('version.h', 'w')
version.write('#define DINEMIC_VERSION "' + package_version + '"\n')
version.write('#define DINEMIC_ARCH "' + arch + '"\n')
version.write('#define DINEMIC_BUILD "' + build_date + '"\n')
version.write('#define DINEMIC_ID "' + str(uuid.uuid4()) + '"\n')
version.close()

#call('tar czf libdinemic-src-' + package_version + '.tgz libdinemic/', shell=True)

rc = call('cmake .', shell=True)
rc = call('make -j ' + str(cores), shell=True)

if not os.path.exists('starsheep'):
    print('Failed to compile binary')
    sys.exit(1)

call('cp starsheep ' + bindir, shell=True)
call('cp config.dinemic ' + etcdir + '/', shell=True)
call('cp starsheep.service ' + systemddir + '/')

for flavor in flavors:
    call('fpm -s dir ' \
                    '-a ' + arch_name + ' ' \
                    '-t ' + flavor + ' ' \
                    '-n starsheep ' \
                    '-v ' + package_version + ' ' \
                    '--description "starsheep" ' \
                    '-d libdinemic ' \
                    '-d redis-server ' \
                    '-m "contact@cloudover.io" ' \
                    '--after-install starsheep.afterinstall.sh ' \
                    '-C ' + builddir + ' ' \
                    '.', shell=True)

call('rm -rf ' + builddir, shell=True)

call('mkdir -p ' + packages_dir + '/' + package_version, shell=True)
call('mv starsheep*.rpm ' + packages_dir + '/' + package_version, shell=True)
call('mv starsheep*.deb ' + packages_dir + '/' + package_version, shell=True)
call('mv starsheep.tar ' + packages_dir + '/' + package_version + '/starsheep-' + package_version + '.tar', shell=True)
call('mv starsheep.sh ' + packages_dir + '/' + package_version + '/starsheep-' + package_version + '.sh', shell=True)

call('cp libdinemic.rpm ' + packages_dir + '/' + package_version + '/', shell=True)
call('cp libdinemic.deb ' + packages_dir + '/' + package_version + '/', shell=True)

call('rm -rf CMakeFiles')

call('unlink ' + packages_dir + '/current', shell=True)
call('ln -s ' + package_version + '/ ' + packages_dir + '/current', shell=True)

if sync == 'True' or sync == 'true' or sync == '1' or sync == 'yes':
    key = os.environ['DINEMIC_KEY']
    key_decoded = base64.decodestring(bytes(key.encode('utf-8')))
    f = open('id_rsa', 'w')
    f.write(str(key_decoded))
    f.close()
    print('Syncing with key: ' + str(key_decoded))
    call('chmod 600 id_rsa')
    endpoint = get_arg('endpoint', 'dinemic@10.100.0.204:/srv/data/www/packages.dinemic.io/projects/starsheep')
    call('rsync -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 22 -i ./id_rsa" -av ' + packages_dir + '/ ' + endpoint)
    call('ls -lha id_rsa')
    call('md5sum id_rsa')
    call('rm ./id_rsa')

