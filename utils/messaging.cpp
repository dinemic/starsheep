#include "messaging.h"

using namespace std;
using namespace Dinemic;


void Messaging::print_success(string action, string details) {
    cout << "{" << endl;
    cout << "  success: \"" << action << "\"," << endl;
    cout << "  details: \"" << details << "\"" << endl;
    cout << "}" << endl;
}

void Messaging::print_error(string action, string details) {
    cout << "{" << endl;
    cout << "  error: \"" << action << "\"," << endl;
    cout << "  details: \"" << details << "\"" << endl;
    cout << "}" << endl;
}

void Messaging::print_status(string action, std::map<string, string> details) {
    cout << "[" << endl;
    int i = 0;
    for (auto detail : details) {
        cout << "  {" << endl;
        cout << "    item: \"" << detail.first << "\"," << endl;
        cout << "    status: \"" << detail.second << "\"" << endl;
        cout << "  }";
        if (i < details.size()-1) {
            cout << ",";
        }
        cout << endl;
        i++;
    }
    cout << "]" << endl;
}

void Messaging::print_dmodel_list(const std::vector<DModel> &objects) {
    cout << "[" << endl;
    for (int i = 0; i < objects.size(); i++) {
        string obj_str = objects[i].serialize(2, 2);
        cout << obj_str;
        if (i < objects.size()-1) {
            cout << ",";
        }
        cout << endl;
    }
    cout << "]";
}

void Messaging::print_string_list(const std::vector<string> &items) {
    cout << "[" << endl;
    for (int i = 0; i < items.size(); i++) {
        cout << items[i];
        if (i < items.size()-1) {
            cout << ",";
        }
        cout << endl;
    }
    cout << "]";
}
