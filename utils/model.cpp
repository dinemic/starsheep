#include "model.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

string Model::get_user_id(DApp *app)
{
    if (app == NULL)
        throw DException("Pointer to DApp is NULL");

    // Get existing user or from app parameter
    vector<string> users = DModel::object_list_owned("User:*");

    if (users.size() == 0 && !app->has_option("user-id")) {
        Messaging::print_error("missing option", "user-id");
        throw DAppCommandFailed("Missing option --user-id");
    }

    string user_id;
    if (app->has_option("user-id"))
        user_id = app->get_option("user-id").values[0];
    else
        user_id = users[0];

    return user_id;
}

string Model::get_owned_user_id(DApp *app)
{
    if (app == NULL)
        throw DException("Pointer to DApp is NULL");

    // Get existing user or from app parameter
    vector<string> users = DModel::object_list_owned("User:*");

    if (users.size() == 0) {
        Messaging::print_error("not found", "user");
        throw DAppCommandFailed("No owned user object");
    }
    return users[0];
}

string Model::get_owned_node_id(StoreInterface *store, SyncInterface *sync)
{
    // Get existing user or from app parameter
    vector<string> nodes = DModel::object_list_owned("Node:*");

    if (nodes.size() == 0) {
        Messaging::print_error("not found", "node");
        throw DAppCommandFailed("No owned node object");
    }
    return nodes[0];
}

vector<string> Model::get_assigned_deployments(string node_id, StoreInterface *store, SyncInterface *sync)
{
    Node node(node_id, store, sync);

    vector<string> results;
    return results;
}
