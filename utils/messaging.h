#ifndef MESSAGING_H
#define MESSAGING_H

#include <string>
#include <iostream>

#include <libdinemic/dmodel.h>

class Messaging
{
public:
    static void print_success(std::string action, std::string details="");
    static void print_status(std::string action, std::map<std::string, std::string> details);
    static void print_error(std::string error, std::string details="");
    static void print_dmodel_list(const std::vector<Dinemic::DModel> &objects);
    static void print_string_list(const std::vector<std::string> &items);
};

#endif // MESSAGING_H
