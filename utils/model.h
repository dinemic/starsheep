#ifndef MODEL_H
#define MODEL_H

#include <string>
#include <vector>

#include <utils/messaging.h>

#include <libdinemic/dapp.h>
#include <libdinemic/dmodel.h>

#include <models/deployment.h>
#include <models/node.h>

class Model
{
public:
    static std::string get_user_id(Dinemic::DApp *app);
    static std::string get_owned_user_id(Dinemic::DApp *app);
    static std::string get_owned_node_id(Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync);
    static std::vector<std::string> get_assigned_deployments(std::string node_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync);
};

#endif // MODEL_H
