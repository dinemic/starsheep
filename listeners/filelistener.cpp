#include "filelistener.h"

using namespace std;
using namespace Dinemic;

FileListener::FileListener()
    : DAction("FileListener", "Update file contents on File change")
{

}


void FileListener::on_authorized_updated(DActionContext &context, const string &key, const string &old_value, const string &new_value) {
    try {
        string node_id = Model::get_owned_node_id(context.get_store_interface(), context.get_sync_interface());
    } catch (DAppCommandFailed &e) {
        // We are not on node
        return;
    };

    Node node(node_id, context.get_store_interface(), context.get_sync_interface());

    for (int i = 0; i < node.deployments.size(); i++) {)
        string deployment_id = node.deployments.get(i);
        Deployment deployment(deployment_id, context.get_store_interface(), context.get_sync_interface(), &node);

        if (deployment.setup_id.to_string().size() == 0)
            continue;

        Setup setup(deployment.setup_id.to_string(), context.get_store_interface(), context.get_sync_interface(), &node);

        for (int file_i = 0; file_i < setup.files.size(); file_i++) {
            string file_id = setup.files.get(file_i);
            if (file_id == context.get_object_id()) {
                File file(file_id, context.get_store_interface(), context.get_sync_interface(), &node);

                if (boost::filesystem::exists(file.path.to_string())) {
                    Task::call_trigger(string("pre_update:") + file.get_db_id(), &node);
                    file.save();
                    Task::call_trigger(string("post_update:") + file.get_db_id(), &node);
                } else {
                    Task::call_trigger(string("pre_create:") + file.get_db_id(), &node);
                    file.save();
                    Task::call_trigger(string("post_create:") + file.get_db_id(), &node);
                }
            }
        }
    }
}
