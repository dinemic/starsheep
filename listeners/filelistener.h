#ifndef FILELISTENER_H
#define FILELISTENER_H

#include <boost/filesystem.hpp>

#include <utils/model.h>

#include <libdinemic/daction.h>

#include <models/node.h>
#include <models/file.h>
#include <models/task.h>
#include <models/deployment.h>


class FileListener : public Dinemic::DAction
{
public:
    FileListener();
    void on_authorized_updated(Dinemic::DActionContext &context, const std::string &key, const std::string &old_value, const std::string &new_value);
};

#endif // FILELISTENER_H
