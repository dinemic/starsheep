#ifndef USER_H
#define USER_H

#include <libdinemic/dapp.h>
#include <libdinemic/dmodel.h>
#include <libdinemic/dfield.h>

#include <utils/messaging.h>
#include <utils/model.h>

class User : public Dinemic::DModel
{
public:
    User(Dinemic::Store::StoreInterface *store,
                Dinemic::Sync::SyncInterface *sync,
                const std::vector<std::string> &authorized_object_ids);
    User(const std::string &db_id,
         Dinemic::Store::StoreInterface *store,
         Dinemic::Sync::SyncInterface *sync,
         Dinemic::DModel *caller=NULL);

    Dinemic::DField name;
    Dinemic::DField email;

    static void command_create(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_list(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_get(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_edit(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_delete(Dinemic::DApp *app, std::vector<std::string> commands);
};

#endif // USER_H
