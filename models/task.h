#ifndef CONFIGTask_H
#define CONFIGTask_H

#include <libdinemic/dmodel.h>
#include <libdinemic/dfield.h>
#include <libdinemic/dapp.h>

#include <utils/messaging.h>
#include <utils/model.h>
#include <models/user.h>

#include <string>
#include <vector>
#include <boost/process.hpp>


class Task : public Dinemic::DModel
{
    std::vector<std::string> execute(std::string deployment_id);

public:
    Task(Dinemic::Store::StoreInterface *store,
               Dinemic::Sync::SyncInterface *sync,
               const std::vector<std::string> &authorized_object_ids);
    Task(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, DModel *caller=NULL);

    /// Contents of script or service definition
    Dinemic::DField script;

    /// How script will be executed. For example:
    /// - /bin/bash
    /// - /usr/bin/python
    /// - systemd (for systemd services)
    Dinemic::DField interpreter;
    Dinemic::DField timeout;
    /// When Task should be called
    /// - pre_create:[file id]
    /// - post_create:[file_id]
    /// - pre_update:[file id]
    /// - post_update:[file_id]
    /// - pre_remove:[file id]
    /// - post_remove:[file_id]
    /// - on_boot
    /// - on_shutdown
    /// - minutely
    /// - hourly:[minute]
    /// - daily:[hour]:[minute]
    /// - weekly:[DOW]:[hour]:[minute]
    /// - monthly:[DOM]:[hour]:[minute]
    /// - systemd
    /// systemd trigger can't be called multiple times
    Dinemic::DList triggers;

    std::vector<std::string> handle_created();
    std::vector<std::string> handle_updated();
    std::vector<std::string> handle_cron();
    std::vector<std::string> handle_boot();
    std::vector<std::string> handle_shutdown();
    std::vector<std::string> handle_systemd();

    static void command_create(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_list(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_get(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_edit(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_delete(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_authorize_user(Dinemic::DApp *app, std::vector<std::string> commands);

    static void call_trigger(std::string event, Node *node);
};

#endif // CONFIGTask_H
