#include "user.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

User::User(const string &db_id, StoreInterface *store, SyncInterface *sync, DModel *caller)
    : DModel(db_id, store, sync, NULL, caller),
      name(this, "name", false),
      email(this, "email", false)
{

}

User::User(StoreInterface *store, SyncInterface *sync, const std::vector<string> &authorized_object_ids)
    : DModel("User", store, sync, authorized_object_ids),
      name(this, "name", false),
      email(this, "email", false)
{

}

void User::command_create(DApp *app, std::vector<string> commands) {
    if (DModel::object_list_owned("User:*").size() > 0) {
        Messaging::print_error("exists", "user");
        throw DAppCommandFailed("User exists. Cannot create multiple user instances on single machine");
    }

    User user(app->get_store_interface(), app->get_sync_interface(), vector<string>());
    Messaging::print_success("created", user.get_db_id());
}

void User::command_list(DApp *app, std::vector<string> commands) {
    Messaging::print_string_list(DModel::object_list("User:*", app->get_store_interface()));
}

void User::command_get(DApp *app, std::vector<string> commands) {
    User user(commands[0], app->get_store_interface(), app->get_sync_interface());

    cout << user.serialize() << endl;
}

void User::command_edit(DApp *app, std::vector<string> commands) {
    User user(commands[0], app->get_store_interface(), app->get_sync_interface());

    if (!user.is_owned()) {
        Messaging::print_error("not found", "user");
        throw DAppCommandFailed("User not owned");
    }

    if (app->has_option("user-email")) {
        user.email = app->get_option("user-email").values[0];
    }

    if (app->has_option("user-name")) {
        user.email = app->get_option("user-name").values[0];
    }

    Messaging::print_success("edited", user.get_db_id());
}

void User::command_delete(DApp *app, std::vector<string> commands) {
    User user(commands[0], app->get_store_interface(), app->get_sync_interface());

    if (!user.is_owned()) {
        Messaging::print_error("not owned", "user");
        throw DAppCommandFailed("User not owned");
    }

    user.remove();
    Messaging::print_success("deleted", user.get_db_id());
}
