#include "task.h"

using namespace Dinemic;
using namespace std;

Task::Task(const string &db_id, Store::StoreInterface *store, Sync::SyncInterface *sync, DModel *caller)
    : DModel(db_id, store, sync, NULL, caller),
      script(this, "script", true),
      interpreter(this, "interpreter", true),
      timeout(this, "timeout", true),
      triggers(this, "triggers", true)
{

}

Task::Task(Store::StoreInterface *store, Sync::SyncInterface *sync, const std::vector<string> &authorized_object_ids)
    : DModel("Task", store, sync, authorized_object_ids),
      script(this, "script", true),
      interpreter(this, "interpreter", true),
      timeout(this, "timeout", true),
      triggers(this, "triggers", true)
{

}

vector<string> Task::execute(string deployment_id) {
    boost::process::ipstream stdout, stderr; //reading pipe-stream
    boost::filesystem::path file_path(boost::filesystem::temp_directory_path());
    file_path /= boost::filesystem::unique_path();

    boost::filesystem::ofstream file{file_path};
    file << script.to_string();
    file.close();

    boost::filesystem::path interpreter_path(interpreter.to_string());
    if (!boost::filesystem::exists(interpreter_path)) {
        //TODO: Error
    }
    boost::process::child c(interpreter.to_string(), file_path, boost::process::std_out > stdout, boost::process::std_err > stderr);

    std::string stdout_data, stderr_data;
    std::string line;

    while (c.running()) {
        if (std::getline(stdout, line) && !line.empty()) {
            stdout_data += line;
        }
        if (std::getline(stderr, line) && !line.empty()) {
            stderr_data += line;
        }
    }

    c.wait();

    vector<string> result;
    result.push_back(stdout_data);
    result.push_back(stderr_data);
    return result;
}

void Task::command_create(DApp *app, std::vector<string> commands) {
    if (!app->has_option("task-script-path") && !app->has_option("task-script")) {
        Messaging::print_error("missing option", "task-script-path or task-script option");
        throw DAppCommandFailed("Missing option --task-script [contents] or --task-script-path [file path]");
    }

    vector<string> users;
    for (string user_id : app->get_option("user-id").values) {
        User user(user_id, app->get_store_interface(), app->get_sync_interface());
        users.push_back(user_id);
    }

    string user_id = Model::get_user_id(app);
    if (std::find(users.begin(), users.end(), user_id) == users.end()) {
        users.push_back(user_id);
    }

    Task task(app->get_store_interface(), app->get_sync_interface(), users);
    task.timeout = app->get_option("task-timeout").values[0];
    task.interpreter = app->get_option("task-interpreter").values[0];

    for (string trigger : app->get_option("task-trigger").values) {
        task.triggers.append(trigger);
    }

    if (app->has_option("task-script")) {
        task.script = app->get_option("task-script").values[0];
    } else if (app->has_option("task-script-path")) {
        throw DException("Unsupported option. TODO");
    }

    Messaging::print_success("created", task.get_db_id());
}

void Task::command_delete(DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);

    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    Task task(app->get_option("task-id").values[0], app->get_store_interface(), app->get_sync_interface(), &user);

    if (!task.is_update_authorized(user_id)) {
        Messaging::print_error("auth", "not authorized");
        throw DAppCommandFailed("Not authorized to delete this object");
    }

    task.remove();
    Messaging::print_success("deleted", task.get_db_id());
}

void Task::command_list(DApp *app, std::vector<string> commands) {
    if (app->has_option("only-owned")) {
        vector<string> objects = DModel::object_list_owned("Task:*");
        Messaging::print_string_list(objects);
    } else {
        vector<string> objects = DModel::object_list("Task:*", app->get_store_interface());
        Messaging::print_string_list(objects);
    }
}

void Task::command_get(DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);
    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    Task object(app->get_option("task-id").values[0], app->get_store_interface(), app->get_sync_interface(), &user);
    cout << object.serialize() << endl;
}

void Task::command_edit(DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);
    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    Task task(app->get_option("task-id").values[0], app->get_store_interface(), app->get_sync_interface(), &user);

    if (!task.is_update_authorized(user_id)) {
        Messaging::print_error("auth", "not authorized");
        throw DAppCommandFailed("Not authorized to update this object");
    }

    if (app->has_option("task-timeout")) {
        task.timeout = app->get_option("task-timeout").values[0];
    }

    if (app->has_option("task-interpreter")) {
        task.interpreter = app->get_option("task-interpreter").values[0];
    }

    if (app->has_option("task-script")) {
        task.script = app->get_option("task-script").values[0];
    }

    if (app->has_option("task-trigger")) {
        int triggers_count = task.triggers.size();
        for (int i = 0; i < triggers_count; i++) {
            task.triggers.del(i);
        }

        for (auto t : app->get_option("task-trigger").values) {
            task.triggers.append(t);
        }
    }

    Messaging::print_success("edited", task.get_db_id() );
}

void Task::command_authorize_user(DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);
    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    Task object(app->get_option("task-id").values[0], app->get_store_interface(), app->get_sync_interface(), &user);

    if (!object.is_update_authorized(user_id)) {
        Messaging::print_error("auth", "not authorized");
        throw DAppCommandFailed("Not authorized to update this object");
    }

    object.update_authorized_objects.append(app->get_option("authorize-user").values[0]);
    Messaging::print_success("authorized");
}
