#include "setup.h"

using namespace std;

Setup::Setup(const string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, DModel *caller)
    : Dinemic::DModel(db_id, store, sync, NULL, caller),
      name(this, "Name", true),
      files(this, "Files", true),
      tasks(this, "Tasks", true)
{

}

Setup::Setup(Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, const std::vector<string> &authorized_object_ids)
    : Dinemic::DModel("Setup", store, sync, authorized_object_ids),
      name(this, "Name", true),
      files(this, "Files", true),
      tasks(this, "Tasks", true)
{

}

void Setup::command_create(Dinemic::DApp *app, std::vector<string> commands) {
    vector<string> users;
    for (string user_id : app->get_option("user-id").values) {
        User user(user_id, app->get_store_interface(), app->get_sync_interface());
        users.push_back(user_id);
    }

    string user_id = Model::get_user_id(app);
    if (std::find(users.begin(), users.end(), user_id) == users.end()) {
        users.push_back(user_id);
    }

    Setup setup(app->get_store_interface(), app->get_sync_interface(), users);

    setup.name = app->get_option("setup-name").values[0];

    for (string task_id : app->get_option("setup-id").values) {
        setup.tasks.append(task_id);
    }

    for (string file_id : app->get_option("file-id").values) {
        setup.files.append(file_id);
    }

    Messaging::print_success("created", setup.get_db_id());
}

void Setup::command_list(Dinemic::DApp *app, std::vector<std::string> commands) {
    if (app->has_option("only-owned")) {
        vector<string> objects = DModel::object_list_owned("Setup:*");
        Messaging::print_string_list(objects);
    } else {
        vector<string> objects = DModel::object_list("Setup:*", app->get_store_interface());
        Messaging::print_string_list(objects);
    }
}

void Setup::command_get(Dinemic::DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);
    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    Setup object(app->get_option("setup-id").values[0], app->get_store_interface(), app->get_sync_interface(), &user);
    cout << object.serialize() << endl;
}

void Setup::command_delete(Dinemic::DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);

    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    map<string, string > status;

    for (string object_id : app->get_option("setup-id").values) {
        Setup object(object_id, app->get_store_interface(), app->get_sync_interface(), &user);

        if (!object.is_update_authorized(user_id)) {
            status[object_id] = "not-authorized";
            continue;
        }

        object.remove();
        status[object_id] = "deleted";
    }
    Messaging::print_status("deleted", status);
}

void Setup::command_assign_to_node(Dinemic::DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);

    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    Setup setup(app->get_option("setup-id").values[0]);

    if (!setup.is_read_authorized(user_id)) {
        Messaging::print_error("not authorized", setup.get_db_id());
        return;
    }

    map<string, string> status;
    for (auto node_id : app->get_option("node-id").values) {
        Node node(node_id, app->get_store_interface(), app->get_sync_interface(), &user);

        if (!node.is_read_authorized(user_id)) {
            status[node_id] = "not-authorized";
            continue;
        }
        if (!node.is_update_authorized(user_id)) {
            status[node_id] = "not-authorized";
            continue;
        }

        setup.append_read_authorized(node_id);

        vector<string> authorized_objects{user_id, node_id, setup.get_db_id()};
        Deployment deployment(app->get_store_interface(), app->get_sync_interface(), authorized_objects);
        deployment.setup_id = setup.get_db_id();
        deployment.node_id = node_id;

        node.deployments.append(deployment.get_db_id())

        status[node_id] = deployment.get_db_id();
    }
}
