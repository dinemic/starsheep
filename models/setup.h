#ifndef SETUP_H
#define SETUP_H

#include <string>
#include <libdinemic/dmodel.h>
#include <libdinemic/dfield.h>
#include <libdinemic/ddict.h>
#include <libdinemic/dlist.h>
#include <libdinemic/dapp.h>

#include <utils/messaging.h>
#include <utils/model.h>

#include <models/user.h>
#include <models/deployment.h>

class Setup : public Dinemic::DModel
{
public:
    Setup(Dinemic::Store::StoreInterface *store,
               Dinemic::Sync::SyncInterface *sync,
               const std::vector<std::string> &authorized_object_ids);
    Setup(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, DModel *caller=NULL);

    Dinemic::DField name;
    Dinemic::DList files;
    Dinemic::DList tasks;

    static void command_create(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_list(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_get(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_delete(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_add_file(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_remove_file(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_add_task(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_remove_task(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_assign_to_node(Dinemic::DApp *app, std::vector<std::string> commands);
};

#endif // SETUP_H
