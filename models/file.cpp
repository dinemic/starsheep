#include "file.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

File::File(const string &db_id, StoreInterface *store, SyncInterface *sync, DModel *caller)
    : DModel(db_id, store, sync, NULL, caller),
      contents(this, "contents", true),
      path(this, "path", true)
{

}

File::File(StoreInterface *store, SyncInterface *sync, const std::vector<string> &authorized_object_ids)
    : DModel("ConfigFile", store, sync, authorized_object_ids),
      contents(this, "contents", true),
      path(this, "path", true)
{

}

void File::command_create(DApp *app, std::vector<string> commands) {
    if (!app->has_option("file-contents") && !app->has_option("file-contents-path")) {
        Messaging::print_error("missing option", "file-contents or file-contents-path option");
        throw DAppCommandFailed("Missing option --file-contents [contents] or --file-contents-path [file path]");
    }

    vector<string> users;
    for (string user_id : app->get_option("user-id").values) {
        User user(user_id, app->get_store_interface(), app->get_sync_interface());
        users.push_back(user_id);
    }

    string user_id = Model::get_user_id(app);
    if (std::find(users.begin(), users.end(), user_id) == users.end()) {
        users.push_back(user_id);
    }

    File file(app->get_store_interface(), app->get_sync_interface(), users);
    file.path= app->get_option("file-path").values[0];
    if (app->has_option("file-contents")) {
        file.contents = app->get_option("file-contents").values[0];
    } else {
        throw DException("Unsupported option. TODO");
    }

    Messaging::print_success("created", file.get_db_id());
}


void File::command_list(DApp *app, std::vector<string> commands) {
    if (app->has_option("owned-only")) {
        vector<string> objects = DModel::object_list_owned("File:*");
        Messaging::print_string_list(objects);
    } else {
        vector<string> objects = DModel::object_list("File:*", app->get_store_interface());
        Messaging::print_string_list(objects);
    }
}

void File::command_get(DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);
    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    File object(commands[0], app->get_store_interface(), app->get_sync_interface(), &user);
    cout << object.serialize() << endl;
}

void File::command_delete(DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);

    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    map<string, string > status;

    for (string object_id : commands) {
        File object(object_id, app->get_store_interface(), app->get_sync_interface(), &user);

        if (!object.is_update_authorized(user_id)) {
            status[object_id] = "not-authorized";
            continue;
        }

        try {
            boost::filesystem::remove(object.path.to_string());
        } catch (exception &e) {
            status[object_id] = "failed";
            continue;
        }

        object.remove();
        status[object_id] = "deleted";
    }

    Messaging::print_status("deleted", status);
}

void File::save() {
    f = ofstream(path.to_string(), ios_base::out);
    f << contents.to_string();
    f.close();
}
