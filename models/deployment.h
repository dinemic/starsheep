#ifndef DEPLOYMENT_H
#define DEPLOYMENT_H

#include <libdinemic/dmodel.h>
#include <libdinemic/ddict.h>
#include <libdinemic/dfield.h>

#include <utils/messaging.h>
#include <utils/model.h>

#include <models/user.h>

class Deployment : public Dinemic::DModel
{
public:
    Deployment(Dinemic::Store::StoreInterface *store,
               Dinemic::Sync::SyncInterface *sync,
               const std::vector<std::string> &update_authorized_object_ids);
    Deployment(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, DModel *caller=NULL);

    Dinemic::DField setup_id;
    Dinemic::DField node_id;
    Dinemic::DDict variables;
    Dinemic::DDict task_outputs;
    Dinemic::DDict file_versions;

    static void command_create(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_list(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_remove(Dinemic::DApp *app, std::vector<std::string> commands);

    static void command_trigger(Dinemic::DApp *app, std::vector<std::string> commands);
};

#endif // DEPLOYMENT_H
