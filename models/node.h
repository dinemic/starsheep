#ifndef CONFIGNODE_H
#define CONFIGNODE_H

#include <libdinemic/dmodel.h>
#include <libdinemic/dlist.h>
#include <libdinemic/ddict.h>
#include <libdinemic/dfield.h>
#include <libdinemic/dapp.h>

#include <utils/messaging.h>
#include <utils/model.h>

#include <models/user.h>
#include <models/file.h>
#include <models/setup.h>

class Node : public Dinemic::DModel
{
public:
    Node(Dinemic::Store::StoreInterface *store,
               Dinemic::Sync::SyncInterface *sync,
               const std::vector<std::string> &authorized_object_ids);
    Node(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, DModel *caller=NULL);

    Dinemic::DField hostname;
    Dinemic::DList deployments;

    static void command_create(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_delete(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_list(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_get(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_authorize_user(Dinemic::DApp *app, std::vector<std::string> commands);

    /// Update file by file_id if owned or all owned files, if file_id is empty string
    void update_file(std::string file_id);
};

#endif // CONFIGNODE_H
