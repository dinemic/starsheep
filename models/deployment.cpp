#include "deployment.h"

using namespace std;
using namespace Dinemic;

Deployment::Deployment(const string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, DModel *caller)
    : Dinemic::DModel(db_id, store, sync, NULL, caller),
      setup_id(this, "setup_id", true),
      node_id(this, "deployment_id", true),
      variables(this, "variables", true),
      task_outputs(this, "task_outputs", true),
      file_versions(this, "file_versions", true)
{

}

Deployment::Deployment(Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, const std::vector<string> &update_authorized_object_ids)
    : Dinemic::DModel("Deployment", store, sync, update_authorized_object_ids),
      setup_id(this, "setup_id", true),
      node_id(this, "deployment_id", true),
      variables(this, "variables", true),
      task_outputs(this, "task_outputs", true),
      file_versions(this, "file_versions", true)
{

}

void Deployment::command_create(DApp *app, std::vector<string> commands) {

}


void Deployment::command_trigger(DApp *app, std::vector<string> commands) {
    /// - pre_create:[file id]
    /// - post_create:[file_id]
    /// - pre_update:[file id]
    /// - post_update:[file_id]
    /// - pre_remove:[file id]
    /// - post_remove:[file_id]
    /// - on_boot
    /// - on_shutdown
    /// - minutely
    /// - hourly:[minute]
    /// - daily:[hour]:[minute]
    /// - weekly:[DOW]:[hour]:[minute]
    /// - monthly:[DOM]:[hour]:[minute]
    /// - systemd

}
