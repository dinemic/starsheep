#include "node.h"

using namespace std;
using namespace Dinemic;
using namespace Dinemic::Store;
using namespace Dinemic::Sync;

Node::Node(const string &db_id, StoreInterface *store, SyncInterface *sync, DModel *caller)
    : DModel(db_id, store, sync, NULL, caller),
      hostname(this, "hostname", true),
      deployments(this, "deployments", true)
{

}

Node::Node(StoreInterface *store, SyncInterface *sync, const std::vector<string> &authorized_object_ids)
    : DModel("Node", store, sync, authorized_object_ids),
      hostname(this, "hostname", true),
      deployments(this, "deployments", true)
{

}

void Node::command_create(DApp *app, std::vector<string> commands) {
    vector<string> users;
    for (string user_id : app->get_option("user-id").values) {
        User user(user_id, app->get_store_interface(), app->get_sync_interface());
        users.push_back(user_id);
    }

    string user_id = Model::get_user_id(app);
    if (std::find(users.begin(), users.end(), user_id) == users.end()) {
        users.push_back(user_id);
    }

    char hn[1000];
    size_t hn_size = 1000;

    Node node(app->get_store_interface(), app->get_sync_interface(), users);
    node.hostname = gethostname(hn, hn_size);

    Messaging::print_success("created", node.get_db_id());
}

void Node::command_list(DApp *app, std::vector<string> commands) {
    if (app->has_option("owned-only")) {
        vector<string> nodes = DModel::object_list_owned("Node:*");
        Messaging::print_string_list(nodes);
    } else {
        vector<string> nodes = DModel::object_list("Node:*", app->get_store_interface());
        Messaging::print_string_list(nodes);
    }
}

void Node::command_get(DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);
    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    Node node(app->get_option("node-id").values[0], app->get_store_interface(), app->get_sync_interface(), &user);
    cout << node.serialize() << endl;
}

void Node::command_delete(DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);

    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    Node node(app->get_option("node-id").values[0], app->get_store_interface(), app->get_sync_interface(), &user);

    if (!node.is_update_authorized(user_id)) {
        Messaging::print_error("auth", "not authorized");
        throw DAppCommandFailed("Not authorized to delete this object");
    }

    node.remove();
    Messaging::print_success("deleted", node.get_db_id());
}

void Node::command_authorize_user(DApp *app, std::vector<string> commands) {
    string user_id = Model::get_user_id(app);
    User user(user_id, app->get_store_interface(), app->get_sync_interface());

    Node object(app->get_option("node-id").values[0], app->get_store_interface(), app->get_sync_interface(), &user);

    if (!object.is_update_authorized(user_id)) {
        Messaging::print_error("auth", "not authorized");
        throw DAppCommandFailed("Not authorized to update this object");
    }

    object.update_authorized_objects.append(app->get_option("authorize-user").values[0]);
    Messaging::print_success("authorized");
}

void Node::update_file(string file_id) {
    for (string deployment_id : DModel::object_list("Deployment:*", this->get_store_interface())) {
        Deployment deployment(deployment_id, get_store_interface(), get_sync_interface(), this);
        if (deployment.node_id.to_string() == get_db_id()) {
            if (!deployment.is_read_authorized(get_db_id())) {
                //throw DAppCommandFailed("Node not authorized to read node " + node_id);
                continue;
            }
            if (!deployment.is_read_authorized(get_db_id())) {
                //throw DAppCommandFailed("Node not authorized to update node " + node_id);
                continue;
            }

            Setup setup(deployment.setup_id, get_store_interface(), get_sync_interface(), this);

            for (int i = 0; i < setup.files.size(); i++) {
                string f_id = setup.files.get(i);
                if (f_id == file_id || file_id.size() == 0) {
                    File file(f_id, get_store_interface(), get_sync_interface(), this);
                    file.save();
                }
            }
        }
    }
}
