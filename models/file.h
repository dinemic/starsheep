#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include <libdinemic/dapp.h>
#include <libdinemic/dmodel.h>
#include <libdinemic/dfield.h>
#include <libdinemic/dlist.h>

#include <models/user.h>
#include <utils/messaging.h>

#include <boost/filesystem.hpp>

class File : public Dinemic::DModel
{
public:
    File(Dinemic::Store::StoreInterface *store,
               Dinemic::Sync::SyncInterface *sync,
               const std::vector<std::string> &authorized_object_ids);
    File(const std::string &db_id, Dinemic::Store::StoreInterface *store, Dinemic::Sync::SyncInterface *sync, DModel *caller=NULL);

    /// Contents of file
    Dinemic::DField contents;

    /// File path in filesystem
    Dinemic::DField path;

    static void command_create(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_list(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_get(Dinemic::DApp *app, std::vector<std::string> commands);
    static void command_delete(Dinemic::DApp *app, std::vector<std::string> commands);

    /// Save contents of file
    void save();
};

#endif // CONFIGFILE_H
