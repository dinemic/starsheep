#include "starsheepapp.h"

using namespace std;
using namespace Dinemic;

StarsheepApp::StarsheepApp(int argc, char **argv)
    : DApp(argc, argv, "StarSheep", "System and configuration management suite")
{
    add_option("", "only-owned", "Limit listing to owned only objects", true, DAppOptionMode_any);
    add_option("", "authorize-user", "Authorize user to modify object", false, DAppOptionMode_any);

    add_option("", "user-email", "User email", false, DAppOptionMode_any);
    add_option("", "user-name", "User name", false, DAppOptionMode_any);
    add_option("u", "user-id", "User ID", false, DAppOptionMode_any);

    add_command(vector<string>{"user", "create"}, &User::command_create, vector<string>{"user-name", "user-email"}, "Create new user. Requires --user-email and --user-name");
    add_command(vector<string>{"user", "edit"}, &User::command_edit, vector<string>{"[user-id]"}, "Change user's details");
    add_command(vector<string>{"user", "delete"}, &User::command_delete, vector<string>{"[user-id]"}, "Remove user");
    add_command(vector<string>{"user", "list"}, &User::command_list, vector<string>(),  "List users");
    add_command(vector<string>{"user", "get"}, &User::command_get, vector<string>{"[user-id]"}, "Print details about single user");

    add_option("", "task-id", "Task ID", false, DAppOptionMode_any);
    add_option("", "task-interpreter", "Command interpreter", false, DAppOptionMode_any);
    add_option("", "task-timeout", "Command timeout", false, DAppOptionMode_any);
    add_option("", "task-trigger", "Defines when task should be executed. Could be used multiple times to execute command on different triggers.", false, DAppOptionMode_any);
    add_option("", "task-script-path", "Path to script. Could be used instead --task-script", false, DAppOptionMode_any);
    add_option("", "task-script", "Contents of script. Could be used instead --task-script-path", false, DAppOptionMode_any);

    add_command(vector<string>{"task", "create"}, &Task::command_create, vector<string>{"task-interpreter", "task-timeout", "task-trigger"}, "Create new task");
    add_command(vector<string>{"task", "list"}, &Task::command_list, vector<string>(), "List tasks");
    add_command(vector<string>{"task", "get"}, &Task::command_get, vector<string>{"[task-id]"}, "Print details of single task");
    add_command(vector<string>{"task", "edit"}, &Task::command_edit, vector<string>{"[task-id]"}, "Change task's details");
    add_command(vector<string>{"task", "authorize-user"}, &Task::command_authorize_user, vector<string>{"[task-id]", "authorize-user"}, "Authorize new user to modify task");
    add_command(vector<string>{"task", "delete"}, &Task::command_delete, vector<string>{"[task-id]"}, "Remove task");

    add_option("", "file-id", "File ID", false, DAppOptionMode_any);
    add_option("", "file-path", "File path", false, DAppOptionMode_any);
    add_option("", "file-contents", "File contents", false, DAppOptionMode_any);
    add_option("", "file-contents-path", "Path to file contents", false, DAppOptionMode_any);

    add_command(vector<string>{"file", "create"}, &File::command_create, vector<string>{"file-path"});
    add_command(vector<string>{"file", "list"}, &File::command_list, vector<string>(), "List files");
    add_command(vector<string>{"file", "get"}, &File::command_get, vector<string>{"[file-id]"}, "Print details of single file");
    add_command(vector<string>{"file", "delete"}, &File::command_delete, vector<string>{"[file-id]"}, "Remove file");

    add_option("", "node-id", "Task ID", false, DAppOptionMode_any);

    add_command(vector<string>{"node", "create"}, &Node::command_create, vector<string>{"user-id"}, "Create new node with at least one administrator");
    add_command(vector<string>{"node", "list"}, &Node::command_list, vector<string>(), "List nodes");
    add_command(vector<string>{"node", "get"}, &Node::command_get, vector<string>{"node-id"}, "Print details of single node");
    add_command(vector<string>{"node", "delete"}, &Node::command_delete, vector<string>{"node-id"}, "Remove node");
    add_command(vector<string>{"node", "authorize-user"}, &Node::command_authorize_user, vector<string>{"node-id", "authorize-user"}, "Authorize new user to modify node");

    add_option("", "setup-id", "Setup ID", false, DAppOptionMode_any);
    add_option("", "setup-name", "Setup name", false, DAppOptionMode_any);
    add_option("", "variable", "Custom deployment environment variable", false, DAppOptionMode_any);

    add_command(vector<string>{"setup", "create"}, &Setup::command_create);
    add_command(vector<string>{"setup", "list"}, &Setup::command_list, vector<string>(), "List setups");
    add_command(vector<string>{"setup", "get"}, &Setup::command_get, vector<string>{"[setup-id]"}, "Print details of single setup");
    add_command(vector<string>{"setup", "delete"}, &Setup::command_delete, vector<string>{"[setup-id]"}, "Remove setup");
    add_command(vector<string>{"setup", "assign-to-node"}, &Setup::command_assign_to_node, vector<string>{"[setup-id]", "node-id"}, "Assign setup to one or many nodes");

    listener_ignore_unsigned.apply(sync, "Task:[id]");
    listener_ignore_unsigned.apply(sync, "Task:[id]:*");
    listener_ignore_unsigned.apply(sync, "File:[id]");
    listener_ignore_unsigned.apply(sync, "File:[id]:*");
    listener_ignore_unsigned.apply(sync, "User:[id]");
    listener_ignore_unsigned.apply(sync, "User:[id]:*");
    listener_ignore_unsigned.apply(sync, "Node:[id]");
    listener_ignore_unsigned.apply(sync, "Node:[id]:*");
    listener_ignore_unsigned.apply(sync, "Setup:[id]");
    listener_ignore_unsigned.apply(sync, "Setup:[id]:*");
}

void StarsheepApp::launch() {

}

void StarsheepApp::create() {

}
