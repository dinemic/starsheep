#include <iostream>

#include <starsheepapp.h>

using namespace std;

int main(int argc, char **argv)
{
    StarsheepApp app(argc, argv);
    return app.exec();
}
