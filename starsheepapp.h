#ifndef STARSHEEPAPP_H
#define STARSHEEPAPP_H

#include <libdinemic/dapp.h>
#include <libdinemic/dapphelpers.h>
#include <models/user.h>
#include <models/task.h>
#include <models/node.h>
#include <models/deployment.h>
#include <models/setup.h>
#include <models/file.h>

class StarsheepApp : public Dinemic::DApp
{
    Dinemic::DAppHelpers::IgnoreUnsignedUpdates listener_ignore_unsigned;
public:
    StarsheepApp(int argc, char **argv);

    void create();
    void launch();
};

#endif // STARSHEEPAPP_H
