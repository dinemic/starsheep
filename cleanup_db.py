import redis


dbs = (11, 12)

for db in dbs:
    r = redis.Redis('localhost', db=db)
    for k in r.keys():
        r.delete(k)
        print("Remove %s from db=%d" % (k, db))
